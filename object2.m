function [ x ] = object2( dt, u )
persistent x1;
persistent x2;

if isempty(x1) 
    x1 = 0;
    x2 = 1;
end

a = 0;
b = 1;
c = 4;
d = 10;
r = 0;%randn / 100;
%x = ((u*c)*dt^2/a+b*x1*dt/a+2*x1-x2)/(1+b*dt) + r;
%x = (c*u*dt^2-x2+x1*(2+a*dt))/(1+a*dt+b+dt^2);
x = (d*u*dt^2-a*x2+x1*(a*2+b*dt))/(a+b*dt+c*dt^2) + r;

%if(x > 100)
%    x = 100;
%end
x2 = x1;
x1 = x;

end

