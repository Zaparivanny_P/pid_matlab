clc
clear all
close all

T = 0.01;

%Kp = 2.5;
%Kip = 3;
%Kdp = .1;

Kp = 1;
Kip = 8;
Kdp = .01;

Ki = Kp*Kip*T;
Kd = Kp*Kdp/T;




U(1) = 0;
U(2) = 0;
E(1) = 10;
E(2) = 10;
r(1) = 0;
r(2) = 0;
t = 0;
for n = 3:200
    %r(i) = object2(t, 2);
    for k = 1:10
        r(n) = object2(T/10, U(n - 1));
    end
    E(n) = 25 - r(n);
    U(n) = U(n - 1) + Kp * (E(n) - E(n-1)) + Ki*E(n) + Kd * (E(n) - 2*E(n - 1) + E(n - 2));

end

plot((1:length(U)), U, (1:length(U)), r);
%figure
%plot(1:length(U), E);



%i = 1;
%for t=1:0.1:10
%    r(i) = object2(t, 2);
%    i = i + 1;
%end

%plot(1:length(r), r);