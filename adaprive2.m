clc
clear all
close all

fs = 2; %������� �����������
g = 25;
delta = .1; %��������
cr = 40; %������ ���������
nu = 1;
r0 = 1; %����


beta1(1) = 0;
beta2(1) = 0;
beta3(1) = 0;

beta1(2) = 0;
beta2(2) = 0;
beta3(2) = 0;

u(1) = 0;
y(1) = 0;
y(2) = 0;
t = 0;
nuUp = 2 * (1 - fs^2/delta^2);
disp(['nu up:' num2str(nuUp)]);

for k = 2:100
    
    u(k) = beta1(k)*y(k) + beta2(k)*y(k-1) + beta3(k) * g;

    for n = 1:10
        y(k + 1) = object2(0.01, u(k));
    end
    
    err = (y(k+1)-g);
    if(abs(err) > delta)
        gr = cr*(y(k)^2 + y(k-1)^2 +g^2);
        beta1(k + 1) = beta1(k) - nu * (r0 * err * y(k)) / gr;
        beta2(k + 1) = beta2(k) - nu * (r0 * err * y(k-1)) / gr;
        beta3(k + 1) = beta3(k) - nu * (r0 * err * g) / gr;
    else
        beta1(k + 1) = beta1(k);
        beta2(k + 1) = beta2(k);
        beta3(k + 1) = beta3(k);
    end

end


plot(1:length(u), u, 1:length(y), y)