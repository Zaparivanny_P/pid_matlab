clc
clear all
close all

fs = 2; %������� �����������
g = 25;
delta = .1; %��������
cr = 30; %������ ���������
nu = 1;
r0 = 1; %����


beta1(1) = 0;
beta2(1) = 0;
u(1) = 0;
y(1) = 0;
t = 0;
nuUp = 2 * (1 - fs^2/delta^2);
disp(['nu up:' num2str(nuUp)]);

for k = 1:200
    
    u(k) = beta1(k)*y(k) + beta2(k) * g;

    for n = 1:10
        y(k + 1) = object2(0.01, u(k));
    end
    
    if(k == 10000)
    %    g = 4;
    end

    if(abs((y(k+1)-g)) > delta)
        beta1(k + 1) = beta1(k) - nu * (r0 * (y(k+1)-g) * y(k)) / (cr*(y(k)^2+g^2));
        beta2(k + 1) = beta2(k) - nu * (r0 * (y(k+1)-g) * g) / (cr*(y(k)^2+g^2));
    else
        beta1(k + 1) = beta1(k);
        beta2(k + 1) = beta2(k);
    end

end


plot(1:length(u), u, 1:length(y), y)